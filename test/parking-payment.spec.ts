import "mocha";
import * as assert from "assert";
import * as moment from "moment";
import {duration, Moment} from "moment";
import {PricingBuilder} from "../src/PricingBuilder";
import {IPricing} from "../src/IPricing";

describe("parking payment", () => {

    let gracePeriodInMinutes = 10;
    let intervalSizeInMinutes = 15;
    let intervalRate = 3;
    let initialBlockRate = 10;
    let initialBlockSizeInMinutes = 60;
    let maxDailyPayment = 80;
    let weekendNightFixedRate = 40;

    let mondayAt6am = asMoment("2018-01-01T06:00:00");
    let mondayAt10am = asMoment("2018-01-01T10:00:00");
    let fridayAt10pm = asMoment("2018-01-05T22:00:00");
    let saturdayAt10am = asMoment("2018-01-06T10:00:00");
    let saturdayAt10pm = asMoment("2018-01-06T22:00:00");

    let gracePeriod = duration(gracePeriodInMinutes, "minutes");
    let initialBlockSize = duration(initialBlockSizeInMinutes, "minutes");
    let intervalSize = duration(intervalSizeInMinutes, "minutes");
    let dayStart = duration(6, "hours");
    let weekendEveningStartTime = duration(22, "hours");

    let pricing: IPricing;

    beforeEach(() => {

        pricing = new PricingBuilder()
            .dayStart(dayStart)
            .gracePeriod(gracePeriod)
            .maxDailyPayment(maxDailyPayment)
            .weekendEveningStartTime(weekendEveningStartTime)
            .incrementSize(intervalSize)
            .incrementRate(3)
            .initialBlockSize(initialBlockSize)
            .initialBlockRate(initialBlockRate)
            .weekendEveningFixedRate(weekendNightFixedRate)
            .weekendDays(5, 6)
            .build();
    });

    describe("grace period pricing", () => {

        it("should pay nothing on grace period", () => {
            verifyExpectedPricing(0, mondayAt10am, almost(endOfGracePeriod(mondayAt10am)));
        });

        it("should pay 'initial block rate' on the grace period mark", () => {
            verifyExpectedPricing(initialBlockRate, mondayAt10am, endOfGracePeriod(mondayAt10am));
        });
    });

    describe("hourly pricing", () => {
        it("should pay 'initial block rate' + 'interval rate' on the 'initial block mark'", () => {
            verifyExpectedPricing(initialBlockRate + intervalRate, mondayAt10am, endOfInitialBlock(mondayAt10am));
        });

        it("should pay 'initial block rate' + 2 * 'interval rate' on the first 'interval mark'", () => {
            verifyExpectedPricing(initialBlockRate + 2 * intervalRate, mondayAt10am, endOfFirstInterval(mondayAt10am));
        });
    });

    describe("nightly pricing", () => {

        it("should pay 'night rate' on weekend nights", () => {
            verifyExpectedPricing(weekendNightFixedRate, fridayAt10pm, endOfGracePeriod(fridayAt10pm));
            verifyExpectedPricing(weekendNightFixedRate, saturdayAt10pm, endOfGracePeriod(saturdayAt10pm));
        });

        it("should pay daily and nightly", () => {
            verifyExpectedPricing(initialBlockRate + weekendNightFixedRate, almost(fridayAt10pm), endOfGracePeriod(fridayAt10pm));
        });
    });


    describe("daily pricing", () => {

        it("should not pay more than 'max daily' per day on weekday", () => {
            verifyExpectedPricing(maxDailyPayment, mondayAt10am, moment(mondayAt10am).add(8, "hours"));
        });

        it("should not pay more than 'max daily' per day on weekend", () => {
            verifyExpectedPricing(maxDailyPayment, saturdayAt10am, endOfGracePeriod(saturdayAt10pm));
        });
    });

    describe("multiple days", () => {

        it("the day starts at 6am", () => {
            verifyExpectedPricing(initialBlockRate * 2,
                moment(mondayAt6am).subtract(5, "minutes"),
                moment(mondayAt6am).add(5, "minutes")
            );
        });

        it("should 'max daily' for a full day", () => {
            verifyExpectedPricing(maxDailyPayment, mondayAt6am, almost(moment(mondayAt6am).add(24, "hours")));
        });

        it("all days", () => {
            verifyExpectedPricing(7 * maxDailyPayment,
                mondayAt6am,
                almost(moment(mondayAt6am).add(7, "days"))
            );
        });
    });


    function verifyExpectedPricing(expected: number, entryTime: Moment, paymentTime: Moment) {
        let amount = pricing.calculatePayment(entryTime, paymentTime);
        assert.strictEqual(amount, expected);
    }

    function asMoment(time: string): moment.Moment {
        return moment(time);
    }

    function almost(aMoment) {
        return moment(aMoment).subtract(1, "millisecond");
    }

    function endOfGracePeriod(aMoment) {
        return moment(aMoment).add(gracePeriod);
    }

    function endOfFirstInterval(aMoment: moment.Moment) {
        return moment(aMoment).add(initialBlockSize).add(intervalSize);
    }

    function endOfInitialBlock(aMoment: moment.Moment) {
        return moment(aMoment).add(initialBlockSize);
    }

});