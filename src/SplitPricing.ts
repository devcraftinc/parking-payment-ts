import * as moment from "moment";
import {Moment} from "moment";
import {IPricing} from "./IPricing";

export class SplitPricing implements IPricing {

    private readonly firstPricing: IPricing;
    private readonly secondPricing: IPricing;
    private readonly splitTime: (entryTime: moment.Moment) => moment.Moment;

    constructor(firstPricing: IPricing, secondPricing: IPricing, splitTime: (entryTime: Moment) => Moment) {
        this.firstPricing = firstPricing;
        this.secondPricing = secondPricing;
        this.splitTime = splitTime;
    }

    public calculatePayment(entryTime: moment.Moment, paymentTime: moment.Moment): number {
        const splitMark = this.splitTime(entryTime);

        let result: number = 0;
        if (entryTime.isBefore(splitMark)) {
            result += this.firstPricing.calculatePayment(entryTime, moment.min(paymentTime, splitMark));
        }

        if (paymentTime.isSameOrAfter(splitMark)) {
            result += this.secondPricing.calculatePayment(moment.max(splitMark, entryTime), paymentTime);
        }
        return result;
    }
}