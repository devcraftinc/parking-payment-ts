import * as moment from "moment";
import {duration} from "moment";
import {IntervalPricing} from "./IntervalPricing";
import {LimitedPricing} from "./LimitedPricing";
import {SplitPricing} from "./SplitPricing";
import {FixedPricing} from "./FixedPricing";
import {IPricing} from "./IPricing";
import {GracePeriodPricing} from "./GracePeriodPricing";
import {DayOfWeekPricing} from "./DayOfWeekPricing";


export class PricingBuilder {

    private _dayStart = duration(6, "hours");
    private _weekendEveningStartTime = duration(22, "hours");
    private _incrementSize = duration(15, "minutes");
    private _initialBlockSize = duration(60, "minutes");
    private _gracePeriod = duration(10, "minutes");
    private _isWeekendDay: boolean[] = [false, false, false, false, false, true, true];

    private _weekendEveningFixedRate = 40;
    private _initialBlockRate = 10;
    private _intervalRate = 3;
    private _maxDailyPayment = 80;

    public build(): IPricing {
        let hourlyPricing = new SplitPricing(new FixedPricing(this._initialBlockRate), new IntervalPricing(this._incrementSize, this._intervalRate),
            (entryTime => moment(entryTime).add(this._initialBlockSize)));
        let weeknightPricing = new FixedPricing(this._weekendEveningFixedRate);
        let weekdayPricing = new LimitedPricing(hourlyPricing, this._maxDailyPayment);
        let weekendPricing = new LimitedPricing(new SplitPricing(hourlyPricing, weeknightPricing, (entryTime => moment(entryTime).startOf("day").add(this._weekendEveningStartTime))), this._maxDailyPayment);

        return new GracePeriodPricing(this._gracePeriod, new FixedPricing(0),
            new DayOfWeekPricing(
                this.selectDailyPricing(weekdayPricing, weekendPricing , 0),
                this.selectDailyPricing(weekdayPricing, weekendPricing , 1),
                this.selectDailyPricing(weekdayPricing, weekendPricing , 2),
                this.selectDailyPricing(weekdayPricing, weekendPricing , 3),
                this.selectDailyPricing(weekdayPricing, weekendPricing , 4),
                this.selectDailyPricing(weekdayPricing, weekendPricing , 5),
                this.selectDailyPricing(weekdayPricing, weekendPricing , 6),
                this._dayStart));
    }

    private selectDailyPricing(weekdayPricing, weekendPricing: IPricing, weekday: number) {
        return this._isWeekendDay[weekday]?weekendPricing:weekdayPricing;
    }

    public dayStart(value: moment.Duration) {
        this._dayStart = value;
        return this;
    }

    public weekendEveningStartTime(value: moment.Duration) {
        this._weekendEveningStartTime = value;
        return this;
    }

    public incrementSize(value: moment.Duration) {
        this._incrementSize = value;
        return this;
    }

    public initialBlockSize(value: moment.Duration) {
        this._initialBlockSize = value;
        return this;
    }

    public gracePeriod(value: moment.Duration) {
        this._gracePeriod = value;
        return this;
    }

    public weekendEveningFixedRate(value: number) {
        this._weekendEveningFixedRate = value;
        return this;
    }

    public initialBlockRate(value: number) {
        this._initialBlockRate = value;
        return this;
    }

    public incrementRate(value: number) {
        this._intervalRate = value;
        return this;
    }

    public maxDailyPayment(value: number) {
        this._maxDailyPayment = value;
        return this;
    }

    // 0 = Sunday ... 6 = Saturday
    public weekendDays(...days:number[]){
        this._isWeekendDay = [false,false,false,false,false,false,false];
        for (let day of days){
            this._isWeekendDay[day] = true;
        }
        return this;
    }
}