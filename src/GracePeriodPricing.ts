import {IPricing} from "./IPricing";
import * as moment from "moment";
import {Duration, Moment} from "moment";

export class GracePeriodPricing implements IPricing {

    constructor(private gracePeriod: Duration, private gracePricing: IPricing, private otherPricing: IPricing) {
    }

    calculatePayment(entryTime: moment.Moment, paymentTime: Moment): number {
        const gracePeriodEnd = moment(entryTime).add(this.gracePeriod);
        if (paymentTime.isBefore(gracePeriodEnd)) {
            return this.gracePricing.calculatePayment(entryTime, paymentTime);
        }
        return this.otherPricing.calculatePayment(entryTime, paymentTime);
    }
}