import * as moment from "moment";
import {Moment} from "moment";

export interface IPricing {
    calculatePayment(entryTime: moment.Moment, paymentTime: Moment): number;
}