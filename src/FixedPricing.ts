import * as moment from "moment";
import {IPricing} from "./IPricing";

export class FixedPricing implements IPricing {
    constructor(private fixedRate: number) {
    }

    public calculatePayment(entryTime: moment.Moment, paymentTime: moment.Moment) {
        return this.fixedRate;
    }
}