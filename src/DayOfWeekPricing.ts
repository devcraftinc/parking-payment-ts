import {IPricing} from "./IPricing";
import * as moment from "moment";
import {duration} from "moment";

export class DayOfWeekPricing implements IPricing {

    private readonly dailyPricing: IPricing[] = [null, null, null, null, null, null, null];
    private readonly dayStart: moment.Duration;

    constructor(
        private sun: IPricing,
        private mon: IPricing,
        private tue: IPricing,
        private wed: IPricing,
        private thu: IPricing,
        private fry: IPricing,
        private sat: IPricing,
        dayStart = duration(6, "hours")) {

        this.dailyPricing[0] = sun;
        this.dailyPricing[1] = mon;
        this.dailyPricing[2] = tue;
        this.dailyPricing[3] = wed;
        this.dailyPricing[4] = thu;
        this.dailyPricing[5] = fry;
        this.dailyPricing[6] = sat;
        this.dayStart = dayStart;
    }

    calculatePayment(entryTime: moment.Moment, paymentTime: moment.Moment): number {
        let splitMark = this.nextDayStart(entryTime);
        let result = 0;
        while (paymentTime.isSameOrAfter(splitMark)) {
            let dailyPricing = this.getDailyPricing(entryTime);
            result += dailyPricing.calculatePayment(entryTime, splitMark);
            entryTime = moment(splitMark);
            splitMark.add(24, "hours");
        }
        let dailyPricing = this.getDailyPricing(entryTime);
        result += dailyPricing.calculatePayment(entryTime, paymentTime);
        return result;
    }

    private getDailyPricing(entryTime: moment.Moment) {
        let weekday = this.nextDayStart(entryTime).isoWeekday() - 1;
        let dailyPricing = this.dailyPricing[weekday];
        return dailyPricing;
    }

    private nextDayStart(entryTime: moment.Moment) {

        let splitMark = moment(entryTime).startOf("day").add(this.dayStart);
        if (splitMark.isSameOrBefore(entryTime))
            splitMark.add(24, "hours");
        return splitMark;
    }
}