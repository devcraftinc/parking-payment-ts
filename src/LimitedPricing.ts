import * as moment from "moment";
import {IPricing} from "./IPricing";

export class LimitedPricing implements IPricing {

    constructor(private limited: IPricing, private maximumPayment: number) {
    }

    calculatePayment(entryTime: moment.Moment, paymentTime: moment.Moment): number {
        return Math.min(this.maximumPayment, this.limited.calculatePayment(entryTime, paymentTime));
    }

}