import * as moment from "moment";
import {Duration} from "moment";

export class IntervalPricing {

    constructor(private intervalSize: Duration, private intervalRate: number) {
    }

    private numOfIntervals(entryTime: moment.Moment, paymentTime: moment.Moment): number {
        const totalTime = paymentTime.diff(entryTime, "milliseconds");
        let intervalInMillis = this.intervalSize.asMilliseconds();
        return 1 + Math.floor((totalTime / intervalInMillis));
    }

    public calculatePayment(oneHourMark, paymentTime: moment.Moment): number {
        return this.numOfIntervals(oneHourMark, paymentTime) * this.intervalRate;
    }
}